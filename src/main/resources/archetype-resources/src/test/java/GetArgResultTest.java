#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
#set( $dt = $symbol_pound.getClass().forName("java.util.Date").newInstance() )
#set( $year = $dt.getYear() + 1900 )
/*
 * Copyright ${year} Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ${package};

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit tests for {@link GetArgResult}.
 */
public final class GetArgResultTest {

    /**
     * Verify that {@link GetArgResult#getArg} returns expected result.
     */
    @Test
    public void getArgTest() {

        GetArgResult target = new GetArgResult(0, "zero");
        assertEquals(0, target.getArg());

    }

      /**
       * Verify that {@link GetArgResult#getValue} returns expected result.
       */
      @Test
      public void getValueTest() {

          GetArgResult target = new GetArgResult(0, "zero");
          assertEquals("zero", target.getValue());

      }
}
