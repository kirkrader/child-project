#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
#set( $dt = $symbol_pound.getClass().forName("java.util.Date").newInstance() )
#set( $year = $dt.getYear() + 1900 )
/*
 * Copyright ${year} Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ${package};

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

/**
 * Unit tests for {@link DemoService}.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { DemoService.class })
@AutoConfigureMockMvc
public final class DemoServiceTest {

    /**
     * Mock Spring MVC test harness.
     */
    @Autowired
    private MockMvc mvc;

    /**
     * Verify that {@link DemoService#getArg} returns the expected value.
     *
     * @throws Exception
     *             Thrown if an unexpected error occurs.
     */
    @Test
    public void getArgTest() throws Exception {

        mvc.perform(MockMvcRequestBuilders.get("/${artifactId}/0")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }

    /**
     * Verify that {@link DemoService#get} returns the expected value.
     *
     * @throws Exception
     *             Thrown if an unexpected error occurs.
     */
    @Test
    public void getTest() throws Exception {

        mvc.perform(MockMvcRequestBuilders.get("/${artifactId}")
                .accept(MediaType.TEXT_PLAIN)).andExpect(status().isOk())
                .andExpect(
                        content().string(equalTo("Hello from ${artifactId}")));

    }
}
