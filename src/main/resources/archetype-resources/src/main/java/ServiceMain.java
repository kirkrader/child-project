#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
#set( $dt = $symbol_pound.getClass().forName("java.util.Date").newInstance() )
#set( $year = $dt.getYear() + 1900 )
/*
 * Copyright ${year} Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ${package};

import static springfox.documentation.builders.PathSelectors.regex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Program entry point.
 *
 * Launches SpringApplication for {@link DemoService}.
 */
@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackageClasses = { DemoService.class })
public class ServiceMain {

    /**
     * Run SpringApplication for this class.
     *
     * @param args
     *            Command-line arguments
     */
    public static void main(final String[] args) {

        SpringApplication.run(ServiceMain.class, args);

    }

    /**
     * Construct API info.
     *
     * @return API info
     */
    private ApiInfo apiInfo() {

        return new ApiInfoBuilder()
                .title("${artifactId} API")
                .description("A 'Hello, world!' API that exists "
                        + "only for demonstration purposes")
                .version("${version}")
                .build();

    }

    /**
     * Construct the Swagger documentation.
     *
     * @return Swagger documentation.
     */
    @Bean
    public Docket demoServiceApi() {

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .paths(regex("/${artifactId}.*"))
                .build();

    }
}
