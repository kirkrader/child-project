#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
#set( $dt = $symbol_pound.getClass().forName("java.util.Date").newInstance() )
#set( $year = $dt.getYear() + 1900 )
/*
 * Copyright ${year} Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ${package};

/**
 * Response from {@link DemoService#getArg}.
 *
 * This bean is used as the return value type for {@link DemoService#getArg} and
 * will be automatically serialized to JSON by the Spring framework. In
 * particular, it is used to convey both the position on the command line and
 * the value of each command line argument originally passed to the JVM when
 * {@link DemoService} was launched.
 */
public final class GetArgResult {

  /**
   * Argument position.
   */
  private int a;

  /**
   * Argument value.
   */
  private String v;

  /**
   * Initialize argument position and value.
   *
   * @param arg Position
   *
   * @param value Value
   */
  public GetArgResult(final int arg, final String value) {

    a = arg;
    v = value;

  }

  /**
   * Get argument position.
   *
   * @return Position
   */
  public int getArg() {

    return a;

  }

  /**
   * Get argument value.
   *
   * @return value
   */
  public String getValue() {

    return v;

  }
}
