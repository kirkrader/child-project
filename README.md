Copyright &copy; 2017 Kirk Rader

# Child Project Archetype

Maven archetype that adds a new module to an existing project as
created by us.rader:parent-project-archetype
