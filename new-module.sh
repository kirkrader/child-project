#!/bin/bash

# Invoke the latest deployed version of
# us.rader:child-project-archetype

# This will add a new module to an existing multi-module project
# created using us.rader:parent-project-archetype -- it must be
# invoked from the base directory of such a project

# Usage: .../new-module.sh artifactId package

# The new module will inherit its groupId and version from the parent
# POM

if [ $# -ne 2 ]; then

    echo "usage: new-module.sh artifiactId package"
    exit 10

fi

mvn archetype:generate \
    -DarchetypeGroupId=us.rader \
    -DarchetypeArtifactId=child-project-archetype \
    -DarchetypeVersion=1.0-SNAPSHOT \
    -DartifactId=$1 \
    -Dpackage=$2 \
    -DinteractiveMode=false
